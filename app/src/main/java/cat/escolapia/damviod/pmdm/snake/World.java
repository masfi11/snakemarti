package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Graphics;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;
    public wall wall;
    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        placeDiamond(1);
        placeDiamond(2);
        placeWall();
    }

    private void placeDiamond(int n) {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }
        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        int diamond2X = random.nextInt(WORLD_WIDTH);
        int diamond2Y = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamond2X][diamond2Y] == false) break;
            diamond2X += 1;
            if (diamond2X >= WORLD_WIDTH) {
                diamond2X = 0;
                diamond2Y += 1;
                if (diamond2Y >= WORLD_HEIGHT) {
                    diamond2Y = 0;
                }
            }
        }
        if(n==1) {
            diamond = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        }
        else if(n==2)
        {
            diamond2=new Diamond(diamond2X,diamond2Y,Diamond.TYPE_1);
        }
    }
    private void placeWall() {
        int WallX = random.nextInt(WORLD_WIDTH);
        int WallY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[WallX][WallY] == false) break;
            WallX += 1;
            if (WallX >= WORLD_WIDTH) {
                WallX = 0;
                WallY += 1;
                if (WallY >= WORLD_HEIGHT) {
                    WallY = 0;
                }
            }
        }
        wall = new wall(WallX, WallY);
    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            if(head.x==diamond.x&&head.y==diamond.y){
                placeDiamond(1);
                score=score+SCORE_INCREMENT;
                snake.allarga();
                tick=tick-TICK_DECREMENT;
            }
            if(head.x==diamond2.x&&head.y==diamond2.y){
                placeDiamond(2);
                score=score+SCORE_INCREMENT;
                snake.allarga();
                tick=tick-TICK_DECREMENT;
            }
            if(head.x==wall.x&&head.y==wall.y){
               gameOver=true;
            }

            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }
}
